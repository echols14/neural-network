package neuralnetwork;

import javafx.util.Pair;
import org.ejml.simple.SimpleMatrix;
import java.util.List;

class BackPropThreadHelper extends Thread {
	//members
	private NNParameters sumGradient = null;
	private final NeuralNetwork nn;
	private final List<Pair<SimpleMatrix, SimpleMatrix>> batch;

	//constructor
	public BackPropThreadHelper(NeuralNetwork nn, List<Pair<SimpleMatrix, SimpleMatrix>> batch) {
		this.nn = nn;
		this.batch = batch;
	}

	@Override
	public void run() {
		sumGradient = nn.backPropogateBatch(batch);
	}

	NNParameters getSumGradient() {
		return sumGradient;
	}
}
