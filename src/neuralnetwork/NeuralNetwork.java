package neuralnetwork;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.util.Pair;
import org.ejml.simple.SimpleMatrix;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class NeuralNetwork {
	//static
	public static final String LOGS = "logs";
	public static final String COST_LOG_CSV = LOGS + File.separator + "multithread_cost_log.csv";
	public static final String ACCURACY_LOG_CSV = LOGS + File.separator + "multithread_accuracy_log.csv";
	private static final Random rand = new Random();
	//data members
	private NNParameters parameters;
	private final double tolerance;
	private final double learningRate;
	private final int batchSize;
	private final boolean printToLog;
	private final boolean printToTerminal;
	private final boolean multiThreaded;

	//constructor
	public NeuralNetwork(int inputSize, int hiddenLayerSize, int outputSize, int hiddenLayers, double tolerance,
	                     double learningRate, int batchSize, boolean printToLog, boolean printToTerminal, boolean multiThreaded) {
		if(hiddenLayers < 1) throw new IllegalArgumentException("Number of hidden layers must be 1 or greater.");
		//by passing a Random object into the constructor, it will be initialized with random values
		parameters = new NNParameters(inputSize, hiddenLayerSize, outputSize, hiddenLayers+1, rand);
		this.tolerance = tolerance;
		this.learningRate = learningRate;
		this.batchSize = batchSize;
		this.printToLog = printToLog;
		this.printToTerminal = printToTerminal;
		this.multiThreaded = multiThreaded;
	}

	private SimpleMatrix calculateError(SimpleMatrix input, SimpleMatrix desiredOutput){
		//verify valid data to train on
		verifyInputVector(input);
		verifyOutputVector(desiredOutput);
		//figure out what the network gets right now
		SimpleMatrix actualOutput = calculate(input);
		//calculate the square of the difference
		return desiredOutput.minus(actualOutput).elementPower(2.0);
	}

	private double calculateErrorScalar(SimpleMatrix input, SimpleMatrix desiredOutput){
		//verify valid data to train on
		verifyInputVector(input);
		verifyOutputVector(desiredOutput);
		//figure out what the network gets right now
		SimpleMatrix actualOutput = calculate(input);
		double magnitude = vectorLength(desiredOutput.minus(actualOutput));
		return (Math.pow(magnitude, 2.0) / 2.0);
	}

	private SimpleMatrix calculateErrorDerivative(SimpleMatrix actualOutput, SimpleMatrix desiredOutput){
		return actualOutput.minus(desiredOutput);
	}

	//train a list of inputs and outputs
	public void train(List<Pair<SimpleMatrix, SimpleMatrix>> trainingDataList){
		if(printToLog) {
			saveToLog("Cost", COST_LOG_CSV);
			saveToLog("Accuracy", ACCURACY_LOG_CSV);
		}
		double costBeforeTraining = costFunction(trainingDataList);
		if(printToTerminal) System.out.println("costBeforeTraining = " + costBeforeTraining);

		//use back-propogation to correct our weights and biases until all changes are very small
		double changeInCost;
		double previousCost = costBeforeTraining;
		double accuracy;
		do { //run an epoch
			//adjust the current weights and biases
			backPropogateByBatches(trainingDataList);
			//check accuracy and cost
			accuracy = runTests(trainingDataList, this);
			double newCost = costFunction(trainingDataList);
			changeInCost = previousCost - newCost;
			previousCost = newCost;
		//} while(previousCost > tolerance);
		//} while(accuracy <= 0.5);
		} while(changeInCost > tolerance);
	}

	private double costFunction(List<Pair<SimpleMatrix, SimpleMatrix>> trainingDataList){
		//calculate the current errors for the given training data
		double costSum = 0.0;
		int exceptionCount = 0;
		for(Pair<SimpleMatrix, SimpleMatrix> dataPair: trainingDataList) {
			try {
				SimpleMatrix actualOutput = calculate(dataPair.getKey());
				SimpleMatrix differenceVector = dataPair.getValue().minus(actualOutput);
				double lengthOfDifferenceVector = vectorLength(differenceVector);
				costSum += Math.pow(lengthOfDifferenceVector, 2.0);
			}
			catch(IllegalArgumentException e){
				//unable to use the given pair of input and output
				exceptionCount++;
				System.err.println(e.toString());
				System.err.println("Training datum could not be used. Continuing...");
			}
		}
		if(exceptionCount == trainingDataList.size()){
			System.err.println("No training data to use.");
			return 0.0;
		}
		if(exceptionCount != 0) System.out.printf("Training data pairs rejected: %d\n", exceptionCount);
		return costSum / (2.0 * (trainingDataList.size() - exceptionCount));
	}

	//this function runs a single epoch
	private void backPropogateByBatches(List<Pair<SimpleMatrix, SimpleMatrix>> trainingDataList){
		int trainingSize = trainingDataList.size();

		//randomize the order that training data will be pulled
		System.out.println("New epoch; re-shuffling batches");
		List<Integer> allIndices = new ArrayList<>(trainingSize);
		for(int i = 0; i < trainingSize; i++){
			allIndices.add(i);
		}
		Collections.shuffle(allIndices, rand);

		//make batches
		List<List<Pair<SimpleMatrix, SimpleMatrix>>> allBatches = new ArrayList<>(1 + trainingSize / batchSize);
		List<Pair<SimpleMatrix, SimpleMatrix>> tempBatch = null;
		for(int i = 0; i < allIndices.size(); i++){
			if(tempBatch == null){ //new batch
				tempBatch = new ArrayList<>(batchSize);
			}
			tempBatch.add(trainingDataList.get(allIndices.get(i)));
			if(tempBatch.size() == batchSize || i == allIndices.size()-1){ //end of this batch
				allBatches.add(tempBatch);
				tempBatch = null;
			}
		}

		if(multiThreaded){
			backPropogateBatchesMultiThreaded(allBatches, trainingSize);
		}
		else {
			//call backPropogate on each batch
			for (List<Pair<SimpleMatrix, SimpleMatrix>> batch : allBatches) {
				//calculate the gradient for this batch
				NNParameters sumGradientVector = backPropogateBatch(batch);
				//use the average gradient from training data to correct our neural network
				applySumGradientToParameters(sumGradientVector, trainingDataList.size());
			}
		}
		//check cost
		double newCost = costFunction(trainingDataList);
		if(printToLog) saveToLog(newCost, COST_LOG_CSV);
		if(printToTerminal) System.out.println("cost = " + newCost);
		//check accuracy
		double accuracy = runTests(trainingDataList, this);
		if(printToLog) saveToLog(accuracy, ACCURACY_LOG_CSV);
		if(printToTerminal) System.out.println("accuracy = " + accuracy);
	}

	private void backPropogateBatchesMultiThreaded(List<List<Pair<SimpleMatrix, SimpleMatrix>>> batches, int numberOfTrainingPairs){
		List<BackPropThreadHelper> threadHelpers = new LinkedList<>();
		for(List<Pair<SimpleMatrix, SimpleMatrix>> batch: batches) {
			BackPropThreadHelper helper = new BackPropThreadHelper(this, batch);
			helper.start();
			threadHelpers.add(helper);
		}

		//establish a sum "gradient vector" starting with 0.0 in every slot
		NNParameters sumGradientVector = getBlankParameters();

		//wait for all calculations to finish
		boolean allFinished;
		do {
			allFinished = true;
			for(int i = 0; i < threadHelpers.size(); i++){
				BackPropThreadHelper helper = threadHelpers.get(i);
				NNParameters gradientVector = helper.getSumGradient();
				if(gradientVector != null){
					//add it to the sum
					sumGradientVector = sumGradientVector.plus(gradientVector);
					//get rid of this ThreadHelper
					threadHelpers.remove(i--);
				}
				else {
					allFinished = false;
				}
			}
		} while (!allFinished);

		//use the average gradient from training data to correct our neural network
		applySumGradientToParameters(sumGradientVector, numberOfTrainingPairs);
	}

	private void applySumGradientToParameters(NNParameters sumGradientVector, int numberOfTrainingPairs){
		//use the average gradient from training data to correct our neural network
		parameters = parameters.minus(sumGradientVector.multiply(learningRate / numberOfTrainingPairs));
	}

	NNParameters backPropogateBatch(List<Pair<SimpleMatrix, SimpleMatrix>> trainingDataList){
		//establish a sum "gradient vector" starting with 0.0 in every slot
		NNParameters sumGradientVector = getBlankParameters();
		for(Pair<SimpleMatrix, SimpleMatrix> trainingPair: trainingDataList) {
			//find the gradient from this single training pair
			NNParameters gradientVector = backPropogateSingle(trainingPair.getKey(), trainingPair.getValue());
			//add it to the sum
			sumGradientVector = sumGradientVector.plus(gradientVector);
		}
		return sumGradientVector;
	}

	private NNParameters backPropogateSingle(SimpleMatrix input, SimpleMatrix desiredOutput){
		final int L = parameters.numberOfTransitions;
		//create a place to store the gradient vector
		NNParameters gradientVector = new NNParameters(parameters.inputSize, parameters.hiddenLayerSize, parameters.outputSize, L);

		//run the input through to get the activation values and weighted sums in each layer
		verifyInputVector(input);
		SimpleMatrix runningVector = input;
		List<SimpleMatrix> activationA = new ArrayList<>(L+1);
		activationA.add(runningVector);
		List<SimpleMatrix> beforeNormalizedZ = new ArrayList<>(L);
		for(int i = 0; i < L; i++) {
			//apply weights and biases
			SimpleMatrix weightsMatrix = parameters.weights.get(i);
			SimpleMatrix biasesVector = parameters.biases.get(i);
			runningVector = weightsMatrix.mult(runningVector).plus(biasesVector);
			beforeNormalizedZ.add(runningVector);
			//normalize each value in the resulting vector
			runningVector = normalize(runningVector);
			activationA.add(runningVector);
		}
		SimpleMatrix finalResult = runningVector; // = activationA.get(L);

		//calculate the final f'
		SimpleMatrix finalBeforeNormalizedZ = beforeNormalizedZ.get(L-1);
		SimpleMatrix fPrimeL = normalizerDerivative(finalBeforeNormalizedZ);

		//calculate the first delta vector
		SimpleMatrix deltaVector = calculateErrorDerivative(finalResult, desiredOutput).elementMult(fPrimeL);
		//apply as a correction to the last set of biases
		for(int row = 0; row < deltaVector.numRows(); row++){
			gradientVector.setBiasValue(L-1, row, deltaVector.get(row));
		}
		//calculate this part of the weight gradient from the delta vector
		SimpleMatrix firstWeightGradient = deltaVector.mult(activationA.get(L-1).transpose());
		//save thisGradient into to overall vector
		for(int row = 0; row < firstWeightGradient.numRows(); row++){
			for(int col = 0; col < firstWeightGradient.numCols(); col++){
				gradientVector.setWeightValue(L-1, row, col, firstWeightGradient.get(row, col));
			}
		}

		//calculate previous delta vectors and gradient
		for(int l = L-2; l >= 0; l--) { //l represents the index of the delta we are about to find
			//calculate the f' vector at this layer
			SimpleMatrix thisBeforeNormalizedZ = beforeNormalizedZ.get(l);
			SimpleMatrix fPrimeVector = normalizerDerivative(thisBeforeNormalizedZ);

			//calculate the next delta vector
			SimpleMatrix thisWeightsMatrix = parameters.weights.get(l+1);
			SimpleMatrix weightsTransposeByPreviousDelta = thisWeightsMatrix.transpose().mult(deltaVector);
			SimpleMatrix thisActivationA = activationA.get(l);
			deltaVector = weightsTransposeByPreviousDelta.elementMult(fPrimeVector);
			//apply as a correction to this set of biases
			for(int row = 0; row < deltaVector.numRows(); row++){
				gradientVector.setBiasValue(l, row, deltaVector.get(row));
			}

			//calculate this part of the weight gradient from the delta vector
			SimpleMatrix thisWeightGradient = deltaVector.mult(thisActivationA.transpose());
			//apply as a correction to this set of weights
			for(int row = 0; row < thisWeightGradient.numRows(); row++){
				for(int col = 0; col < thisWeightGradient.numCols(); col++){
					gradientVector.setWeightValue(l, row, col, thisWeightGradient.get(row, col));
				}
			}
		}

		//return the gradient from this particular training pair
		return gradientVector;
	}

	//for a given input, calculate the output
	public SimpleMatrix calculate(SimpleMatrix inputVector){
		SimpleMatrix runningVector = inputVector;
		verifyInputVector(runningVector);
		for(int i = 0; i < parameters.weights.size(); i++) {
			//apply weights and biases
			SimpleMatrix weightsMatrix = parameters.weights.get(i);
			SimpleMatrix biasesVector = parameters.biases.get(i);
			runningVector = weightsMatrix.mult(runningVector).plus(biasesVector);
			//normalize each value in the resulting vector
			runningVector = normalize(runningVector);
		}
		return runningVector;
	}

	//check that an input vector is the right size
	private void verifyInputVector(SimpleMatrix inputVector){
		if(inputVector.numCols() != 1){
			throw new IllegalArgumentException("The inputVector must be a vector, not a full matrix.");
		}
		if(inputVector.numRows() != parameters.inputSize){
			throw new IllegalArgumentException("The inputVector is of length " + inputVector.numRows() + " but " + parameters.inputSize + " was expected.");
		}
	}

	//check that an output vector is the right size
	private void verifyOutputVector(SimpleMatrix outputVector){
		if(outputVector.numCols() != 1){
			throw new IllegalArgumentException("The outputVector must be a vector, not a full matrix.");
		}
		if(outputVector.numRows() != parameters.outputSize){
			throw new IllegalArgumentException("The outputVector is of length " + outputVector.numRows() + " but " + parameters.outputSize + " was expected.");
		}
	}

	private static SimpleMatrix normalize(SimpleMatrix vector){
		if(vector.numCols() != 1){
			throw new IllegalArgumentException("The outputVector must be a vector, not a full matrix.");
		}
		SimpleMatrix toReturn = new SimpleMatrix(vector.numRows(), 1);
		for(int j = 0; j < vector.numRows(); j++) {
			double newVal = normalize(vector.get(j));
			toReturn.set(j, newVal);
		}
		return toReturn;
	}

	private static SimpleMatrix normalizerDerivative(SimpleMatrix matrix){
		SimpleMatrix toReturn = new SimpleMatrix(matrix.numRows(), matrix.numCols());
		for(int row = 0; row < matrix.numRows(); row++){
			for(int col = 0; col < matrix.numCols(); col++){
				toReturn.set(row, col, normalizerDerivative(matrix.get(row, col)));
			}
		}
		return toReturn;
	}

	private static double normalize(double z){
		//if this function is changed, its derivative needs to be re-calculated an re-incorporated into back-propogation
		return 1.0/(1.0 + Math.exp(-z));
	}

	private static double normalizerDerivative(double z){
		//return (Math.exp(-z) * Math.pow(1.0 + Math.exp(-z), -2.0));
		double normalizedZ = normalize(z);
		return (normalizedZ * (1.0 - normalizedZ));
	}

	public String serialize(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static NeuralNetwork deserialize(String jsonNeuralNetwork){
		Gson gson = new Gson();
		return gson.fromJson(jsonNeuralNetwork, NeuralNetwork.class);
	}

	public static double runTests(List<Pair<SimpleMatrix, SimpleMatrix>> testingData, NeuralNetwork nn){
		int successCount = 0;
		int totalCount = 0;
		for(Pair<SimpleMatrix, SimpleMatrix> testingDatum : testingData) {
			SimpleMatrix input = testingDatum.getKey();
			SimpleMatrix expectedOutput = testingDatum.getValue();
			//try to calculate the answer
			SimpleMatrix actualOutput = nn.calculate(input);
			if (actualOutput.numCols() == 1) {
				totalCount++;
			}
			else {
				System.err.println("One of the testing results was a full matrix, not a vector as expected");
				continue;
			}
			if (findMaxIndex(expectedOutput) == findMaxIndex(actualOutput)) {
				successCount++;
			}
		}
		return ((double) successCount) / ((double) totalCount);
	}

	public static int findMaxIndex(SimpleMatrix vector){
		if(vector.numCols() != 1){
			throw new IllegalArgumentException("The argument was a full matrix, not a vector as expected");
		}
		double maxVal = -1.0;
		int maxIndex = -1;
		for(int i = 0; i < vector.numRows(); i++) {
			double thisVal = vector.get(i, 0);
			if(thisVal > maxVal){
				maxVal = thisVal;
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	public static double vectorLength(SimpleMatrix vector){
		if(vector.numCols() != 1){
			throw new IllegalArgumentException("The argument was a full matrix, not a vector as expected");
		}
		double sumOfSquares = 0.0;
		for(int i = 0; i < vector.numRows(); i++) {
			double thisVal = vector.get(i, 0);
			sumOfSquares += Math.pow(thisVal, 2.0);
		}
		return Math.pow(sumOfSquares, 0.5);
	}

	private static void saveToLog(String value, String filename){
		try {
			FileWriter log = new FileWriter(new File(filename));
			log.append(value);
			log.append('\n');
			log.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.err.println("Could not write to log");
		}
	}

	private static void saveToLog(Double value, String filename){
		try {
			FileWriter log = new FileWriter(new File(filename), true);
			log.append(value.toString());
			log.append('\n');
			log.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.err.println("Could not write to log");
		}
	}

	private NNParameters getBlankParameters(){
		return new NNParameters(parameters.inputSize, parameters.hiddenLayerSize, parameters.outputSize, parameters.numberOfTransitions);
	}
}
