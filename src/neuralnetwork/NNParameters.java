package neuralnetwork;

import org.ejml.simple.SimpleMatrix;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class NNParameters {
	//data members
	final int inputSize;
	final int hiddenLayerSize;
	final int outputSize;
	final int numberOfTransitions;
	List<SimpleMatrix> weights;
	List<SimpleMatrix> biases;

	//constructor
	NNParameters(int inputSize, int hiddenLayerSize, int outputSize, int numberOfTransitions) {
		this.inputSize = inputSize;
		this.hiddenLayerSize = hiddenLayerSize;
		this.outputSize = outputSize;
		this.numberOfTransitions = numberOfTransitions;
		weights = new ArrayList<>(numberOfTransitions);
		biases = new ArrayList<>(numberOfTransitions);

		Random rand = new Random();

		//generate the first transition
		SimpleMatrix firstWeightMatrix = SimpleMatrix.random(hiddenLayerSize, inputSize, 0.0, 0.0, rand);
		weights.add(firstWeightMatrix);
		SimpleMatrix firstBiasesVector = SimpleMatrix.random(hiddenLayerSize, 1, 0.0, 0.0, rand);
		biases.add(firstBiasesVector);
		//generate the middle n-2 transitions
		for(int i = 1; i < numberOfTransitions - 1; i++) {
			SimpleMatrix middleWeightMatrix = SimpleMatrix.random(hiddenLayerSize, hiddenLayerSize, 0.0, 0.0, rand);
			weights.add(middleWeightMatrix);
			SimpleMatrix middleBiasesVector = SimpleMatrix.random(hiddenLayerSize, 1, 0.0, 0.0, rand);
			biases.add(middleBiasesVector);
		}
		//generate the last transition
		SimpleMatrix lastWeightMatrix = SimpleMatrix.random(outputSize, hiddenLayerSize, 0.0, 0.0, rand);
		weights.add(lastWeightMatrix);
		SimpleMatrix lastBiasesVector = SimpleMatrix.random(outputSize, 1, 0.0, 0.0, rand);
		biases.add(lastBiasesVector);
	}

	NNParameters(int inputSize, int hiddenLayerSize, int outputSize, int numberOfTransitions, Random rand) {
		this(inputSize, hiddenLayerSize, outputSize, numberOfTransitions);
		for(int l = 0; l < numberOfTransitions; l++){
			for(int row = 0; row < weights.get(l).numRows(); row++){
				for(int col = 0; col < weights.get(l).numCols(); col++){
					setWeightValue(l, row, col, rand.nextGaussian());
				}
				setBiasValue(l, row, rand.nextGaussian());
			}
		}
	}

	//other functions

	void setWeightValue(int layer, int row, int column, double value){
		weights.get(layer).set(row, column, value);
	}

	void setBiasValue(int layer, int row, double value){
		biases.get(layer).set(row, value);
	}

	NNParameters plus(NNParameters second){
		if(second.isDimensionMismatch(inputSize, hiddenLayerSize, outputSize, numberOfTransitions)){
			throw new IllegalArgumentException("The two NNParameters objects to add do not have matching dimensions.");
		}
		List<SimpleMatrix> secondWeights = second.weights;
		List<SimpleMatrix> secondBiases = second.biases;
		NNParameters toReturn = new NNParameters(inputSize, hiddenLayerSize, outputSize, numberOfTransitions);
		for(int layer = 0; layer < numberOfTransitions; layer++) {
			SimpleMatrix theseWeights = weights.get(layer);
			SimpleMatrix thoseWeights = secondWeights.get(layer);
			SimpleMatrix theseBiases = biases.get(layer);
			SimpleMatrix thoseBiases = secondBiases.get(layer);
			for(int row = 0; row < theseWeights.numRows(); row++) {
				double biasSum = theseBiases.get(row) + thoseBiases.get(row);
				toReturn.setBiasValue(layer, row, biasSum);
				for(int column = 0; column < theseWeights.numCols(); column++){
					double weightSum = theseWeights.get(row, column) + thoseWeights.get(row, column);
					toReturn.setWeightValue(layer, row, column, weightSum);
				}
			}
		}
		return toReturn;
	}

	NNParameters minus(NNParameters second){
		if(second.isDimensionMismatch(inputSize, hiddenLayerSize, outputSize, numberOfTransitions)){
			throw new IllegalArgumentException("The two NNParameters objects to subtract do not have matching dimensions.");
		}
		List<SimpleMatrix> secondWeights = second.weights;
		List<SimpleMatrix> secondBiases = second.biases;
		NNParameters toReturn = new NNParameters(inputSize, hiddenLayerSize, outputSize, numberOfTransitions);
		for(int layer = 0; layer < numberOfTransitions; layer++) {
			SimpleMatrix theseWeights = weights.get(layer);
			SimpleMatrix thoseWeights = secondWeights.get(layer);
			SimpleMatrix theseBiases = biases.get(layer);
			SimpleMatrix thoseBiases = secondBiases.get(layer);
			for(int row = 0; row < theseWeights.numRows(); row++) {
				double biasDifference = theseBiases.get(row) - thoseBiases.get(row);
				toReturn.setBiasValue(layer, row, biasDifference);
				for(int column = 0; column < theseWeights.numCols(); column++){
					double weightDifference = theseWeights.get(row, column) - thoseWeights.get(row, column);
					toReturn.setWeightValue(layer, row, column, weightDifference);
				}
			}
		}
		return toReturn;
	}

	NNParameters multiply(double factor){
		NNParameters toReturn = new NNParameters(inputSize, hiddenLayerSize, outputSize, numberOfTransitions);
		for(int layer = 0; layer < numberOfTransitions; layer++) {
			SimpleMatrix theseWeights = weights.get(layer);
			SimpleMatrix theseBiases = biases.get(layer);
			for(int row = 0; row < theseWeights.numRows(); row++) {
				double biasProduct = theseBiases.get(row) * factor;
				toReturn.setBiasValue(layer, row, biasProduct);
				for(int column = 0; column < theseWeights.numCols(); column++){
					double weightProduct = theseWeights.get(row, column) * factor;
					toReturn.setWeightValue(layer, row, column, weightProduct);
				}
			}
		}
		return toReturn;
	}

	NNParameters divide(double denominator){
		NNParameters toReturn = new NNParameters(inputSize, hiddenLayerSize, outputSize, numberOfTransitions);
		for(int layer = 0; layer < numberOfTransitions; layer++) {
			SimpleMatrix theseWeights = weights.get(layer);
			SimpleMatrix theseBiases = biases.get(layer);
			for(int row = 0; row < theseWeights.numRows(); row++) {
				double biasDivision = theseBiases.get(row) / denominator;
				toReturn.setBiasValue(layer, row, biasDivision);
				for(int column = 0; column < theseWeights.numCols(); column++){
					double weightDivision = theseWeights.get(row, column) / denominator;
					toReturn.setWeightValue(layer, row, column, weightDivision);
				}
			}
		}
		return toReturn;
	}

	private boolean isDimensionMismatch(int inputSizeIn, int hiddenLayerSizeIn, int outputSizeIn, int numberOfLayersIn){
		return !(inputSizeIn == inputSize &&
				hiddenLayerSizeIn == hiddenLayerSize &&
				outputSizeIn == outputSize &&
				numberOfLayersIn == numberOfTransitions);
	}
}
