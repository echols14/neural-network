import javafx.util.Pair;
import org.ejml.simple.SimpleMatrix;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

class MNIST_FileReader {
	static final int NUMBER_OF_LABEL_TYPES = 11;
	private final String imagesFileName;
	private final String labelsFileName;
	private int imageHeight;
	private int imageWidth;

	public MNIST_FileReader(String imagesFileName, String labelsFileName) {
		this.imagesFileName = imagesFileName;
		this.labelsFileName = labelsFileName;
	}

	public List<Pair<SimpleMatrix, SimpleMatrix>> readFiles(){
		try(BufferedInputStream imagesFile = new BufferedInputStream(new FileInputStream(new File(imagesFileName)));
		    BufferedInputStream labelsFile = new BufferedInputStream(new FileInputStream(new File(labelsFileName)))) {
			//header in the images file
			/*int magicNumberImages =*/ read32bitInt(imagesFile);
			int numberOfImages = read32bitInt(imagesFile);
			imageHeight = read32bitInt(imagesFile);
			imageWidth = read32bitInt(imagesFile);

			//header in the labels file
			/*int magicNumberLabels =*/ read32bitInt(labelsFile);
			int numberOfLabels = read32bitInt(labelsFile);
			if(numberOfImages != numberOfLabels){
				System.err.println("Mismatch between the number of images and labels.");
				return null;
			}

			//read all images with their labels
			List<Pair<SimpleMatrix, SimpleMatrix>> toReturn = new ArrayList<>(numberOfImages);
			for(int imageNumber = 0; imageNumber < numberOfImages; imageNumber++) {
				SimpleMatrix imageVector = readImage(imagesFile);
				int label = labelsFile.read();
				SimpleMatrix labelVector = labelToLabelVector(label);
				toReturn.add(new Pair<>(imageVector, labelVector));
			}
			return toReturn;
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("There was a problem finding one of the provided files.");
			return null;
		}
		catch(IOException e) {
			e.printStackTrace();
			System.err.println("There was a problem reading from the provided files.");
			return null;
		}
	}

	private SimpleMatrix readImage(BufferedInputStream imagesFile) throws IOException{
		SimpleMatrix imageVector = new SimpleMatrix(imageHeight*imageWidth, 1);
		int indexInImage = 0;
		for(int row = 0; row < imageHeight; row++) {
			for(int col = 0; col < imageWidth; col++) {
				int pixel = imagesFile.read();
				double normalizedPixel = pixel / 255.0;
				imageVector.set(indexInImage++, normalizedPixel);
			}
		}
		return imageVector;
	}

	private static int read32bitInt(BufferedInputStream stream) throws IOException {
		byte[] byteArray = new byte[4];
		int bytesRead = stream.read(byteArray, 0, 4);
		if(bytesRead != 4){
			System.err.println("There was a problem reading a 32-bit int from a file.");
		}
		return ByteBuffer.wrap(byteArray).getInt();
	}

	private static SimpleMatrix labelToLabelVector(int label){
		SimpleMatrix labelVector = new SimpleMatrix(NUMBER_OF_LABEL_TYPES, 1);
		for(int i = 0; i < NUMBER_OF_LABEL_TYPES; i++) {
			if(i == label){
				labelVector.set(i, 1.0);
			}
			else{
				labelVector.set(i, 0.0);
			}
		}
		return labelVector;
	}
}
