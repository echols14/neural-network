import javafx.util.Pair;
import neuralnetwork.NeuralNetwork;
import org.ejml.simple.SimpleMatrix;
import java.io.*;
import java.util.List;

public class DigitRecognizer {
	//constants
	private static final double TOLERANCE = 0.00001;
	private static final double LEARNING_RATE = 3.0;
	//private static final int BATCH_SIZE = 128;
	//private static final int BATCH_SIZE = 64;
	private static final int BATCH_SIZE = 30;
	public static final boolean PRINT_TO_LOG = true;
	public static final boolean PRINT_TO_TERMINAL = true;
	//members
	private static NeuralNetwork nn = null;
	private static String fileNameToSave = "no_name_provided.txt";
	private static boolean saved = false;
	private static List<Pair<SimpleMatrix, SimpleMatrix>> trainingData = null;

	public static void main(String[] args){
		//no args
		if(args.length == 0){
			printUsage();
			return;
		}

		//perform desired function
		switch(args[0]) {
			case "load":
				if (args.length == 1) {
					//needs a file name to load from
					printUsage();
					return;
				}
				String neuralNetworkFileName = args[1];
				loadNeuralNetworkFromFile(neuralNetworkFileName);
				break;

			case "new":
				if (args.length == 1) {
					//needs a file name to save to
					printUsage();
					return;
				}
				fileNameToSave = args[1];
				//load training data
				System.out.println("Loading training data...");
				trainingData = loadTrainingData();
				if (trainingData == null) {
					System.err.println("Training data not loaded properly; aborting construction of neural network");
					return;
				}
				System.out.printf("There are %d data pairs for training\n", trainingData.size());
				//set up the emergency save in case something happens
				setAbortWatcher();
				//construct and train the network
				//nn = new NeuralNetwork((28*28), 16, MNIST_FileReader.NUMBER_OF_LABEL_TYPES, 2, 0.00015);
				nn = new NeuralNetwork((28 * 28), 30, MNIST_FileReader.NUMBER_OF_LABEL_TYPES, 1,
						TOLERANCE, LEARNING_RATE, BATCH_SIZE, PRINT_TO_LOG, PRINT_TO_TERMINAL, true);
				System.out.println("Training the neural network...");
				nn.train(trainingData);
				//save the neural network to a file
				saveNeuralNetworkToFile();
				break;

			case "continue":
				if (args.length == 1) {
					//needs a file name to load
					printUsage();
					return;
				}
				fileNameToSave = args[1];
				//load the neural network
				loadNeuralNetworkFromFile(fileNameToSave);
				if(nn == null){ //no neural network loaded
					return;
				}
				//load training data
				trainingData = loadTrainingData();
				//set up the emergency save in case something happens
				setAbortWatcher();
				//set the neural network training again
				nn.train(trainingData);
				break;

			default:
				printUsage();
				return;
		}

		if(nn == null){
			System.err.println("No neural network found to run tests.");
			return;
		}

		//run test data
		System.out.println("Loading testing data...");
		//load testing data
		List<Pair<SimpleMatrix, SimpleMatrix>> testingData = loadTestData();
		if(testingData == null){
			System.err.println("No testing data loaded to run tests.");
			return;
		}
		System.out.println("Run some tests...");
		double finalAccuracy = NeuralNetwork.runTests(testingData, nn);
		System.out.println("finalAccuracy = " + finalAccuracy);
	}

	private static List<Pair<SimpleMatrix, SimpleMatrix>> loadTrainingData(){
		MNIST_FileReader reader = new MNIST_FileReader("mnist/training/train-images.idx3-ubyte", "mnist/training/train-labels.idx1-ubyte");
		return reader.readFiles();
	}

	private static List<Pair<SimpleMatrix, SimpleMatrix>> loadTestData(){
		MNIST_FileReader reader = new MNIST_FileReader("mnist/test/t10k-images.idx3-ubyte", "mnist/test/t10k-labels.idx1-ubyte");
		return reader.readFiles();
	}

	private static void printUsage(){
		System.out.println("Please provide command-line arguments in one of the following ways:");
		System.out.println("new [filenameToCreate]");
		System.out.println("load [filenameToLoad]");
		System.out.println("continue [filenameToContinue]");
	}

	public static void saveNeuralNetworkToFile() {
		if(saved || nn == null){
			return;
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileNameToSave)));
			writer.write(nn.serialize());
			writer.close();
			saved = true;
			System.out.printf("Neural network saved to file %s\n", fileNameToSave);
		}
		catch (IOException e) {
			e.printStackTrace();
			System.err.println("Couldn't properly write to a file to save the neural network.");
		}
	}

	private static void loadNeuralNetworkFromFile(String neuralNetworkFileName){
		try {
			System.out.printf("Loading neural network from file %s\n", neuralNetworkFileName);
			BufferedReader reader = new BufferedReader(new FileReader(new File(neuralNetworkFileName)));
			//read the stuff in the file
			StringBuilder stringifiedNN = new StringBuilder();
			try {
				while(reader.ready()) {
					stringifiedNN.append(reader.readLine());
					stringifiedNN.append('\n');
				}
				reader.close();
			}
			catch (IOException e) {
				e.printStackTrace();
				System.err.println("Could not properly read file to load NeuralNetwork");
				return;
			}
			//make the neural network
			nn = NeuralNetwork.deserialize(stringifiedNN.toString());
			System.out.println("Neural network loaded successfully.");
		}
		catch (FileNotFoundException e) {
			System.err.println("File could not be found to load the neural network.");
		}
	}

	private static void setAbortWatcher() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			/** This handler will be called on Control-C pressed */
			@Override
			public void run() {
				saveNeuralNetworkToFile();
			}
		});
	}
}
