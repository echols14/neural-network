package neuralnetwork;

import javafx.util.Pair;
import org.ejml.simple.SimpleMatrix;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class NeuralNetworkTest {
	//members
	private NeuralNetwork nn = null;
	private List<Pair<SimpleMatrix, SimpleMatrix>> trainingData = null;
	private static final double TOLERANCE = 0.00001;
	private static final double LEARNING_RATE = 1.0;
	private static final int BATCH_SIZE = 4;
	private static final boolean PRINT_TO_LOG = false;
	private static final boolean PRINT_TO_TERMINAL = true;

	@BeforeEach
	void setUp() {
		nn = new NeuralNetwork(2, 4, 4, 2, TOLERANCE, LEARNING_RATE,
				BATCH_SIZE, PRINT_TO_LOG, PRINT_TO_TERMINAL, false);

		trainingData = new ArrayList<>(4);
		//first pair
		double[][] tempArrayIn1 = {{2},{0}};
		SimpleMatrix input = new SimpleMatrix(tempArrayIn1);
		double[][] tempArrayOut1 = {{1},{0},{0},{0}};
		SimpleMatrix output = new SimpleMatrix(tempArrayOut1);
		trainingData.add(new Pair<>(input,output));
		//second pair
		double[][] tempArrayIn2 = {{0},{7}};
		input = new SimpleMatrix(tempArrayIn2);
		double[][] tempArrayOut2 = {{0},{1},{0},{0}};
		output = new SimpleMatrix(tempArrayOut2);
		trainingData.add(new Pair<>(input,output));
		//third pair
		double[][] tempArrayIn3 = {{-2.5},{0}};
		input = new SimpleMatrix(tempArrayIn3);
		double[][] tempArrayOut3 = {{0},{0},{1},{0}};
		output = new SimpleMatrix(tempArrayOut3);
		trainingData.add(new Pair<>(input,output));
		//fourth pair
		double[][] tempArrayIn4 = {{0},{-3}};
		input = new SimpleMatrix(tempArrayIn4);
		double[][] tempArrayOut4 = {{0},{0},{0},{1}};
		output = new SimpleMatrix(tempArrayOut4);
		trainingData.add(new Pair<>(input,output));

		//fifth pair
		double[][] tempArrayIn5 = {{6},{-1}};
		input = new SimpleMatrix(tempArrayIn5);
		double[][] tempArrayOut5 = {{1},{0},{0},{0}};
		output = new SimpleMatrix(tempArrayOut5);
		trainingData.add(new Pair<>(input,output));
		//sixth pair
		double[][] tempArrayIn6 = {{1},{7}};
		input = new SimpleMatrix(tempArrayIn6);
		double[][] tempArrayOut6 = {{0},{1},{0},{0}};
		output = new SimpleMatrix(tempArrayOut6);
		trainingData.add(new Pair<>(input,output));
		//seventh pair
		double[][] tempArrayIn7 = {{-2},{0.5}};
		input = new SimpleMatrix(tempArrayIn7);
		double[][] tempArrayOut7 = {{0},{0},{1},{0}};
		output = new SimpleMatrix(tempArrayOut7);
		trainingData.add(new Pair<>(input,output));
		//eighth pair
		double[][] tempArrayIn8 = {{1},{-4}};
		input = new SimpleMatrix(tempArrayIn8);
		double[][] tempArrayOut8 = {{0},{0},{0},{1}};
		output = new SimpleMatrix(tempArrayOut8);
		trainingData.add(new Pair<>(input,output));
	}
	@AfterEach
	void tearDown() {
		nn = null;
		trainingData = null;
	}

	@Test
	void train() {
		nn.train(trainingData);
		double accuracy = NeuralNetwork.runTests(trainingData, nn);
		System.out.println("Final accuracy = " + accuracy);
		//
		double[][] tempArrayIn1 = {{3},{-0.5}};
		SimpleMatrix input = new SimpleMatrix(tempArrayIn1);
		double[][] tempArrayOut1 = {{1},{0},{0},{0}};
		SimpleMatrix expectedOutput = new SimpleMatrix(tempArrayOut1);
		trainingData.add(new Pair<>(input,expectedOutput));
		SimpleMatrix output = nn.calculate(input);
		assertEquals(NeuralNetwork.findMaxIndex(expectedOutput), NeuralNetwork.findMaxIndex(output));
	}
}