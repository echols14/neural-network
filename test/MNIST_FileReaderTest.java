import javafx.util.Pair;
import neuralnetwork.NeuralNetwork;
import org.ejml.simple.SimpleMatrix;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

class MNIST_FileReaderTest {
	private Random rand = new Random();
	private MNIST_FileReader mnistReader = null;

	@BeforeEach
	void setUp() {
		mnistReader = new MNIST_FileReader("mnist/training/train-images.idx3-ubyte", "mnist/training/train-labels.idx1-ubyte");
	}

	@AfterEach
	void tearDown() {
		mnistReader = null;
	}

	@Test
	void readFiles() {
		List<Pair<SimpleMatrix, SimpleMatrix>> trainingData = mnistReader.readFiles();
		Pair<SimpleMatrix, SimpleMatrix> testPair = trainingData.get(rand.nextInt(trainingData.size()));
		SimpleMatrix imageAsVector = testPair.getKey();
		try {
			BufferedWriter imageFile = new BufferedWriter(new FileWriter(new File("image.csv")));
			int index = 0;
			for(int row = 0; row < 28; row++){
				for(int col = 0; col < 28; col++){
					double value = imageAsVector.get(index++);
					imageFile.write(Double.toString(value));
					imageFile.write(',');
				}
				imageFile.newLine();
			}
			imageFile.close();
		}
		catch (IOException e) {
			System.err.println("Error writing to the image file");
			e.printStackTrace();
		}
		int maxIndex = NeuralNetwork.findMaxIndex(testPair.getValue());
		System.out.printf("Index marked as correct answer: %d\n", maxIndex);
	}
}